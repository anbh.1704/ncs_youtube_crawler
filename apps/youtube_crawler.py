import time

from selenium import webdriver
from selenium.webdriver.common.by import By

from crawler_logger import CrawlerLogger
from decorators import timing_decorator
from utils import (
    DetailCrawler,
    HomePageCrawler,
    YoutubeUtil,
    InteractOption,
    StringHandler,
    GChromeDriver,
)

YOUTUBE_HOMEPAGE_URL = "https://www.youtube.com"
BROWSER_LANGUAGE = "en"
HASHTAG_SEARCH_URL = "https://www.youtube.com/hashtag"

crawler_logger = CrawlerLogger()


class YoutubeCrawlerTool:
    def __init__(self, username=None, password=None):
        self.driver_factory = GChromeDriver
        self.driver = self.driver_factory.init_driver(username, password)
        self.driver.set_window_size(1024, 3000)
        self.detail_crawler = DetailCrawler(self.driver)
        self.string_handler = StringHandler
        self.homepage_crawler = HomePageCrawler(self.driver)
        self.util = YoutubeUtil(self.driver)
        self.username = username
        self.password = password

    @timing_decorator
    def run_script1(self):
        """
        Lấy lượng lớn video link ở homepage  => Crawl dữ liệu của từng Video
        """
        list_video_info = self.homepage_crawler.get_all_relate_video_info()
        detail_list = []
        for video_info in list_video_info:
            detail_list.append(self.detail_crawler.run(video_info["link"]))

        self._export_to_json(detail_list)
        self.driver.quit()

    def run_script2(self, interact_option, keyword_list=[]):
        """
        Tìm theo keyword=> Lấy lượng lớn Video link ở màn đó
        => Crawl dữ liệu của từng Video
        """
        total_video = 0
        detail_list = []
        for keyword in keyword_list:
            video_links = self._get_videos_links_by_keyword(keyword)
            total_video += len(video_links)

            crawler_logger.info(f"Total video: {total_video}")

            try:
                detail_list.append(
                    self.script2_scrape_video(video_links, keyword, interact_option)
                )
            except Exception as e:
                crawler_logger.error(str(e))
        self.driver.quit()

    @timing_decorator
    def run_script3(self, channel_url_list=[]):
        """
        Crawl video của từng channel
        Input: List chứa các Channel
        """
        total_video = 0
        detail_list = []
        for channel_url in channel_url_list:
            video_links = self._get_videos_links_by_channel_url(channel_url)
            total_video += len(video_links)

            crawler_logger.info(f"Total video: {total_video}")
            try:
                detail_list.append(
                    self.channel_url_scrape_video(video_links, channel_url)
                )
            except Exception as e:
                crawler_logger.error((str(e)))

    @timing_decorator
    def run_script6(self, interact_option, hashtag_list=[]):
        """
        Crawl toàn bộ video theo list Hashtag
        """
        total_video = 0
        detail_list = []
        for hashtag in hashtag_list:
            # video_links = self._get_videos_links_by_hashtag(hashtag)
            video_links = self._get_videos_links_by_hashtag_v2(hashtag)
            total_video += len(video_links)
            crawler_logger.info(f"Total video: {total_video}")
            try:
                detail_list.append(
                    self.hashtag_scrape_video(
                        interact_option=interact_option,
                        video_links=video_links,
                        hashtag=hashtag,
                    )
                )
            except Exception as e:
                crawler_logger.error((str(e)))

        result = {"total_video": total_video, "youtube_data": detail_list}
        crawler_logger.info(f"Total Video: {total_video}")
        self._export_to_json(result)
        self.driver.quit()

    def script2_scrape_video(self, video_links, keyword, interact_option):
        data_by_keyword_dict = {keyword: []}
        for index, video_link in enumerate(video_links):
            try:
                crawler_logger.info(
                    f"Processing keyword: {keyword} ({index + 1}/{len(video_links)})"
                )
                video_info = self.detail_crawler.run(
                    video_link, keyword, interact_option
                )
                if video_info is None:
                    continue
                data_by_keyword_dict[keyword].append(video_info)

            except Exception as e:
                crawler_logger.error(str(e))
        return data_by_keyword_dict

    def channel_url_scrape_video(self, video_links, channel_url):
        data_by_channel_dict = {channel_url: []}
        for index, video_link in enumerate(video_links):
            try:
                data_by_channel_dict[channel_url].append(
                    self.detail_crawler.run(video_link)
                )
                crawler_logger.info(
                    f"Processing keyword: {channel_url} ({index + 1}/{len(video_links)})"
                )
            except Exception as e:
                crawler_logger.error(str(e))
        return data_by_channel_dict

    def hashtag_scrape_video(self, interact_option, video_links, hashtag):
        data_by_channel_dict = {hashtag: []}
        self.driver.switch_to.window(self.driver.window_handles[1])
        for index, video_link in enumerate(video_links):
            try:
                crawler_logger.info(
                    f"Processing keyword: {hashtag} ({index + 1}/{len(video_links)})"
                )
                data_by_channel_dict[hashtag].append(
                    self.detail_crawler.run(
                        interact_option=interact_option, video_url=video_link
                    )
                )
            except Exception as e:
                crawler_logger.error(str(e))
        self.driver.switch_to.window(self.driver.window_handles[0])
        return data_by_channel_dict

    def report_video(self, video_link):
        self.driver.get(video_link)
        time.sleep(2)
        self.util.report()

    def _get_videos_links_by_keyword(self, keyword, scroll_time=30):
        format_keyword = "+".join(keyword.split())
        search_url = f"https://www.youtube.com/results?search_query={format_keyword}"
        self.driver.get(search_url)
        try:
            # Recently Update Click
            filter_xpath = '//*[@id="chips"]/yt-chip-cloud-chip-renderer[6]'
            video_filter = self.driver.find_element(By.XPATH, filter_xpath)
            video_filter.click()
            self.detail_crawler.scroll_down_action(scroll_time)
            return self.extract_link_from_page()
        except Exception as e:
            crawler_logger.error(str(e))
            return []

    def _get_videos_links_by_channel_url(self, channel_url, scroll_time=30):
        self.driver.get(channel_url)
        time.sleep(3)

        if self.username and self.password:
            try:
                self.util.subscribe_channel()
            except Exception as e:
                crawler_logger.error(str(e))

        filter_xpath = '//*[@id="tabsContent"]/tp-yt-paper-tab[2]'
        video_filter = self.driver.find_element(By.XPATH, filter_xpath)
        video_filter.click()
        self.detail_crawler.scroll_down_action(scroll_time)
        time.sleep(3)
        return self.extract_link_from_page()

    def _get_videos_links_by_hashtag(self, hashtag, scroll_time=50):
        self.driver.get(f"{HASHTAG_SEARCH_URL}/{hashtag}")
        time.sleep(2)
        self.detail_crawler.scroll_down_action(scroll_time)
        time.sleep(2)
        return self.extract_link_from_page()

    def _get_videos_links_by_hashtag_v2(self, hashtag):
        self.driver.get(f"{HASHTAG_SEARCH_URL}/{hashtag}")
        # hash_tag_info = self.driver.find_element(By.XPATH, '//*[@id="hashtag-info-text"]')
        # info_text = hash_tag_info.text
        # total_videos = self.string_handler.extract_hashtag_views(info_text)
        crawled_rows_text = []
        crawled_video_links = []
        can_scroll = True
        # while len(crawled_video_links) < 100:
        while can_scroll:
            rows = self.driver.find_elements(
                By.XPATH, '//*[@id="contents"]/ytd-rich-grid-row'
            )
            new_rows = [row for row in rows if row.text not in crawled_rows_text]

            if len(new_rows) == 0:
                can_scroll = False
                continue

            for row in new_rows:
                thumbnails = row.find_elements(
                    By.CSS_SELECTOR, ".style-scope .ytd-rich-item-renderer"
                )
                for thumbnail in thumbnails:
                    links = thumbnail.find_elements(By.CSS_SELECTOR, "a#thumbnail")
                    video_links = {
                        link.get_attribute("href")
                        for link in links
                        if link.get_attribute("href") not in crawled_video_links
                    }
                    crawled_video_links.extend(video_links)
                crawled_rows_text.append(row.text)
            self.detail_crawler.scroll_down_action(3)
            time.sleep(5)
        return crawled_video_links

    def extract_link_from_page(self):
        video_links_xpath = (
            '//a[contains(@href, "/watch?v=") and not(contains(@href, "shorts"))]'
        )
        video_links_info = self.driver.find_elements(By.XPATH, video_links_xpath)
        video_links = {link.get_attribute("href") for link in video_links_info}
        return list(video_links)

    def _export_to_json(self, detail_list):
        pass
        # detail_json_string = json.dumps(detail_list, indent=4)
        # with open(JSON_PATH, "w", encoding="utf-8") as json_file:
        #     json_file.write(detail_json_string)




class YoutubeCrawlerJob:
    @staticmethod
    def run_script1(username, password):
        tool = YoutubeCrawlerTool(username=username, password=password)
        tool.run_script1()

    @staticmethod
    def run_script2(interact_option, username=None, password=None, keywords=[]):
        tool = YoutubeCrawlerTool(username=username, password=password)
        tool.run_script2(keyword_list=keywords, interact_option=interact_option)

    @staticmethod
    def run_script3(username, password, channel_urls):
        tool = YoutubeCrawlerTool(username=username, password=password)
        tool.run_script3(channel_urls)

    @staticmethod
    def run_script6(interact_option, username, password, hashtag_list):
        tool = YoutubeCrawlerTool(username=username, password=password)
        tool.run_script6(hashtag_list=hashtag_list, interact_option=interact_option)


if __name__ == "__main__":
    interact_option = InteractOption(
        like_mode=True,
        report_mode=True,
        comment_mode=True,
        comment_sample_list=["Ok", "Good content"],
    )
    YoutubeCrawlerJob.run_script6(
        interact_option=interact_option,
        username="ncs.crawler.bot@gmail.com",
        password="an845919",
        hashtag_list=["Amee", "Bray"],
    )
