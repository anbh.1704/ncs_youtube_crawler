import json
import re
import time

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from selenium import webdriver
from crawler_logger import CrawlerLogger
from result import Post

YOUTUBE_HOMEPAGE_URL = "https://www.youtube.com"
LOGIN_URL = (
    "https://accounts.google.com/InteractiveLogin/identifier?continue=https%3A%2F%2Fwww.youtube.com"
    "%2Fsignin%3Faction_handle_signin%3Dtrue%26app%3Ddesktop%26hl%3Dvi%26next%3Dhttps%253A%252F%252F"
    "www.youtube.com%252F&ec=65620&hl=vi&passive=true&service=youtube&uilel=3&ifkv=AXo7B7U1ikC89cjgyX"
    "oCLIqMxweO4G9w_-_ZilwLmu3PfZDXlw0JcdQP3wM4Y9lEhwb_BwgAEtO2&flowName=GlifWebSignIn&flowEntry=ServiceLogin"
)
crawler_logger = CrawlerLogger()


class InteractOption:
    def __init__(self, like_mode=False, report_mode=False, comment_mode=False, comment_sample_list=None):
        self.like_mode = like_mode
        self.report_mode = report_mode
        self.comment_mode = comment_mode
        self.comment_sample_list = comment_sample_list


class HomePageCrawler:
    def __init__(self, driver):
        self.driver = driver

    def get_all_relate_video_info(self):
        self.driver.get(YOUTUBE_HOMEPAGE_URL)
        time.sleep(5)
        for _ in range(2):
            self.driver.execute_script(
                "window.scrollTo(0, document.body.scrollHeight);"
            )
            time.sleep(3)

        video_info_list = []
        video_thumbnails = self.driver.find_elements(
            "css selector", "a#video-title-link"
        )

        for thumbnail in video_thumbnails:
            video_info = {
                "link": thumbnail.get_attribute("href"),
                "title": thumbnail.get_attribute("title"),
            }
            video_info_list.append(video_info)
        return video_info_list


class DetailCrawler:
    def __init__(self, driver):
        self.driver = driver
        self.driver.execute_script("window.open('', '_blank');")
        driver.switch_to.window(driver.window_handles[0])
        self.util = YoutubeUtil(self.driver)

    def scroll_down_action(self, amount):
        body = self.driver.find_element("tag name", "body")
        for _ in range(amount):
            body.send_keys(Keys.PAGE_DOWN)
            time.sleep(1)

    def extract_video_info(self, interact_option, keyword=None):
        if interact_option:
            self.util.handle_interact_option(interact_option)
        time.sleep(3)
        dropdown_button = self.driver.find_element(By.XPATH, '//*[@id="trigger"]')
        dropdown_button.click()
        time.sleep(2)
        second_option = self.driver.find_element(By.XPATH, '//*[@id="menu"]/a[2]')
        second_option.click()
        video_info = {}
        comment_element = self.driver.find_element(
            By.XPATH, '//*[@id="count"]/yt-formatted-string/span[1]'
        )
        video_info["comment"] = self.comment_validate(comment_element.text)
        current_time = time.time()
        video_info["time_crawl"] = json.dumps(
            current_time, default=lambda x: x.isoformat()
        )
        video_info["link"] = self.driver.current_url
        video_info["id"] = self.extract_video_id(video_info["link"])
        video_info["title"] = self.driver.find_element(
            By.XPATH, '//*[@id="title"]/h1/yt-formatted-string'
        ).text
        video_info["description"] = self._extract_description()

        if keyword:
            is_not_jamming = self.check_jamming_video(
                video_info.get("title"), video_info.get("description"), keyword
            )
            if not is_not_jamming:
                return None
        video_info["type"] = "Youtube"
        video_info["domain"] = "www.youtube.com"
        likes_element = self.driver.find_element(
            By.XPATH,
            '//*[@id="segmented-like-button"]/ytd-toggle-button-renderer/yt-button-shape/button',
        )
        video_info["like"] = self.extract_like_count(
            likes_element.get_attribute("aria-label")
        )
        (
            video_info["author"],
            video_info["author_link"],
        ) = self._extract_channel_name_and_link()

        (
            video_info["view"],
            video_info["created_time"],
        ) = self._extract_views_and_upload_info()

        video_info["duration"] = self.driver.execute_script(
            'return document.querySelector(".ytp-time-duration").textContent;'
        )
        self.scroll_down_action(2)
        time.sleep(2)
        video_info["hashtag"] = self.extract_hashtags(video_info["description"])
        return Post(**video_info)

    def comment_validate(self, comment_count):
        comment_count = comment_count.replace(",", "")
        comment_count = int(comment_count.replace(".", ""))
        return comment_count

    def extract_like_count(self, like_access_name):
        pattern = r"(\d{1,3}(?:,\d{3})*|\d+)"
        match = re.search(pattern, like_access_name)
        if match:
            number_with_commas = match.group(1)
            return int(number_with_commas.replace(",", ""))
        return None

    def extract_video_id(self, video_link):
        pattern = r"(?:watch\?v=)([a-zA-Z0-9_-]{11})"
        match = re.search(pattern, video_link)
        if match:
            return match.group(1)
        return None

    def preprocess(self, text):
        return re.findall(r"\w+", text.lower())

    def create_index(self, documents):
        index = {}
        for doc_id, text in enumerate(documents):
            for word in self.preprocess(text):
                if word not in index:
                    index[word] = []
                index[word].append(doc_id)
        return index

    def is_exist(self, index, word):
        query_words = self.preprocess(word)
        for word in query_words:
            if word in index:
                return True
        return False

    def check_jamming_video(self, title, description, keyword):
        short_description = self.driver.find_element(
            By.XPATH, '//*[@id="bottom-row"]'
        ).text
        index = self.create_index([short_description, description, title])
        return self.is_exist(index, keyword)

    def extract_comments(self, video_id, comment_count):
        comment_elements = self.driver.find_elements(By.CSS_SELECTOR, "#body")
        time.sleep(2)

        while len(comment_elements) < comment_count:
            prev_len = len(comment_elements)
            self.scroll_down_action(3)
            time.sleep(3)
            comment_elements = self.driver.find_elements(By.CSS_SELECTOR, "#body")

            #Nếu lượng comment vẫn như cũ sau khi Scroll => Break (Vì tổng thừa còn lại là Sub comment)
            if len(comment_elements) == prev_len:
                break

        time.sleep(3)
        comments_data = []
        for comment in comment_elements:
            time.sleep(2)
            try:
                author_element = comment.find_element(By.ID, "author-text")
                comment_time_element = comment.find_element(
                    By.CSS_SELECTOR, "#header .published-time-text a"
                )
                comment_text_element = comment.find_element(
                    By.CSS_SELECTOR, "#content-text"
                )
                author_id = author_element.get_attribute("href").split("/")[-1]
                # author_name = author_element.text
                author_link = author_element.get_attribute("href")
                comment_time = comment_time_element.text
                comment_text = comment_text_element.text

                raw_comment_data = {
                    "author": author_id,
                    # "author_name": author_name,
                    'time_crawl': time.time(),
                    "author_link": author_link,
                    "created_time": comment_time,
                    "content": comment_text,
                    "source_id": video_id
                }

                comments_data.append(
                    Post(**raw_comment_data)
                )
            except Exception as e:
                pass
                # crawler_logger.error(str(e))

        return comments_data

    def _extract_channel_name_and_link(self):
        channel_link = self.driver.find_element(
            By.XPATH, '//span[@itemprop="author"]/link'
        ).get_attribute("href")
        json_text = self.driver.find_element(
            By.XPATH, '//script[@type="application/ld+json"]'
        ).get_attribute("textContent")
        json_data = json.loads(json_text)
        channel_name = json_data["itemListElement"][0]["item"]["name"]
        return channel_link, channel_name

    def _extract_description(self):
        time.sleep(2)
        short_description = self.driver.find_element(By.XPATH, '//*[@id="bottom-row"]')
        short_description.click()
        time.sleep(2)
        description = self.driver.find_element(
            By.XPATH, '//*[@id="description-inline-expander"]/yt-attributed-string'
        )
        return description.text.strip()

    def detect_views_and_upload_info(self, context):
        patterns = [
            r"([\d,]+)\s+views\s+([A-Za-z]{3})\s+(\d{1,2}),\s+(\d{4})",
            r"([\d,.]+)\s+lượt xem\s+(\d{1,2})\s+thg\s+(\d{1,2}),\s+(\d{4})",
            r"([\d,]+)\s+views\s+Premiered\s+([A-Za-z]{3})\s+(\d{1,2}),\s+(\d{4})",
            r"([\d.]+)\s+lượt xem\s+Đã công chiếu vào\s+(\d{1,2})\s+thg\s+(\d{1,2}),\s+(\d{4})",
            r"([\d.]+)\s+lượt xem\s+Đã phát trực tiếp vào\s+(\d{1,2})\s+thg\s+(\d{1,2}),\s+(\d{4})",
        ]

        for pattern in patterns:
            match = re.search(pattern, context, re.UNICODE)
            if match:
                views = match.group(1).replace(",", "").replace(".", "")
                day = match.group(2).zfill(2)
                month = match.group(3).zfill(2)
                year = match.group(4)

                date = f"{day}/{month}/{year}"
                return views, date
        crawler_logger.error(context)
        return None, None

    def _extract_views_and_upload_info(self):
        short_description = self.driver.find_element(By.XPATH, '//*[@id="bottom-row"]')
        time.sleep(4)
        short_description.click()
        time.sleep(4)
        context = self.driver.find_element(By.XPATH, '//*[@id="info-container"]')
        return self.detect_views_and_upload_info(context.text.strip())

    def extract_hashtags(self, description):
        context = self.driver.find_element(By.XPATH, '//*[@id="info-container"]')
        short_description_hashtags = re.findall(r"#\w+", context.text.strip())
        description_hashtags = re.findall(r"#\w+", description)
        hashtags = list(set(short_description_hashtags) | set(description_hashtags))
        return hashtags

    def run(self, video_url, interact_option=None, keyword=None):
        try:
            self.driver.get(video_url)
            time.sleep(2)
            video_info = self.extract_video_info(interact_option, keyword)

            if video_info is None:
                return None
            time.sleep(2)

            # Comment crawl độc lập
            # comments = self.extract_comments(video_info.id, video_info.comment)
            data = {
                video_url: {
                    "video_info": video_info,
                }
            }
            return data
        except Exception as e:
            crawler_logger.error(str(e) + video_url)


class YoutubeUtil:
    def __init__(self, driver):
        self.driver = driver

    def skip_ads(self):  # Loading
        try:
            skip_ad = self.driver.find_element(
                By.XPATH, '//*[@id="skip-button:n"]/span/button'
            )
            skip_ad.click()
            time.sleep(5)
        except Exception as e:
            time.sleep(1)

    def subscribe_channel(self):
        subcribe_button = self.driver.find_element(
            By.XPATH, "//*[@id='subscribe-button-shape']/button"
        )
        if subcribe_button.text:
            subcribe_button.click()
            crawler_logger.info(f"Subcribe to channel: {self.driver.current_url}")
        else:
            crawler_logger.info(f"Subcribed before: {self.driver.current_url}")

    def like_video(self):
        like_button = self.driver.find_element(
            By.XPATH,
            "//*[@id='segmented-like-button']/ytd-toggle-button-renderer/yt-button-shape/button",
        )
        isLike = like_button.get_attribute("aria-pressed")
        if isLike == "false":
            crawler_logger.info(f"Like: {self.driver.current_url}")
            like_button.click()
        else:
            crawler_logger.info(f"Liked berfore: {self.driver.current_url}")

    def comment(self, text):
        try:
            comment_box = self.driver.find_element(
                By.XPATH, "//div[@id='placeholder-area']"
            )
            comment_box.click()
            time.sleep(2)
            comment_input = self.driver.find_element(
                By.XPATH, '//*[@id="contenteditable-root"]'
            )
            comment_input.send_keys(text)
            time.sleep(2)

            submit_button = self.driver.find_element(
                By.XPATH, '//*[@id="submit-button"]/yt-button-shape/button'
            )
            submit_button.click()
            crawler_logger.info(f"Commented at {self.driver.current_url}")
            time.sleep(3)
        except Exception as e:
            crawler_logger.error(
                f"Comment error: {str(e)} at {self.driver.current_url}"
            )

    def report(self):
        option_button = self.driver.find_element(
            By.XPATH, '//*[@id="button-shape"]/button/yt-touch-feedback-shape/div'
        )
        option_button.click()
        time.sleep(4)
        report_button = self.driver.find_element(
            By.XPATH, '//*[@id="items"]/ytd-menu-service-item-renderer[3]'
        )

        report_button.click()
        time.sleep(3)
        reason_checkbox = self.driver.find_element(
            By.XPATH,
            '//*[@id="yt-options-renderer-options"]/tp-yt-paper-radio-button[2]',
        )
        reason_checkbox.click()
        time.sleep(4)
        reason_dropdown = self.driver.find_element(
            By.XPATH,
            '//*[@id="yt-options-renderer-options"]/tp-yt-paper-dropdown-menu[2]',
        )
        reason_dropdown.click()

        desired_option_text = "Adults fighting"
        # desired_option_text = 'Cảnh tấn công vào cơ thể'
        options = self.driver.find_elements(By.CSS_SELECTOR, "tp-yt-paper-item")

        for option in options:
            if option.text.strip() == desired_option_text:
                option.click()
                break

        time.sleep(4)
        submit_button = self.driver.find_element(By.XPATH, '//*[@id="submit-button"]')
        submit_button.click()
        time.sleep(3)
        confirm_report_button = self.driver.find_element(
            By.XPATH, '//*[@id="submit-button"]/yt-button-renderer/yt-button-shape/button'
        )
        confirm_report_button.click()
        time.sleep(2)
        close_noti_button = self.driver.find_element(
            By.XPATH, '//*[@id="confirm-button"]/yt-button-shape/button'
        )
        close_noti_button.click()
        time.sleep(2)

    def handle_interact_option(self, interact_option: InteractOption):
        if interact_option.like_mode:
            self.like_video()
            time.sleep(2)
        if interact_option.report_mode:
            try:
                self.report()
                crawler_logger.info("Successfully Reported")
            except Exception as e:
                crawler_logger.error("Fail to report")
            time.sleep(2)
        if interact_option.comment_mode:
            for text in interact_option.comment_sample_list:
                self.comment(text)
                time.sleep(3)

class StringHandler:

    @staticmethod
    def extract_hashtag_views(input_string):
        pattern = r'(\d+\.\d+|\d+)([KMB])?\s*videos'
        match = re.search(pattern, input_string)

        if match:
            # Extract the matched value and unit
            matched_value = float(match.group(1))
            unit = match.group(2)

            # Convert the value based on the unit if it exists
            if unit == 'K':
                approximate_videos = matched_value * 1000
            elif unit == 'M':
                approximate_videos = matched_value * 10 ** 6  # 'M' represents millions
            elif unit == 'B':
                approximate_videos = matched_value * 10 ** 9
            else:
                approximate_videos = matched_value
            return approximate_videos
        return None


class GChromeDriver:

    @classmethod
    def init_driver(cls, username=None, password=None):
        options = webdriver.ChromeOptions()
        options.add_experimental_option("detach", False)
        options.add_argument("--log-level=3")
        # options.add_argument("--headless")
        options.add_argument("--lang=fr")
        options.add_argument("--mute-audio")
        options.add_argument("--disable-infobars")
        options.add_argument("--disable-gpu")
        options.add_argument("--disable-frame-rate-limit --max-frame-rate=30")
        options.add_argument("--disable-popup-blocking")
        options.add_argument("--start-maximize")
        driver = webdriver.Chrome(options=options)
        if username and password:
            GChromeDriver.login(driver, username, password)
        return driver

    @classmethod
    def login(cls, driver, username, password):
        driver.get(LOGIN_URL)
        username_field = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="identifierId"]'))
        )
        # username_field = driver.find_element(By.XPATH, '//*[@id="identifierId"]')
        username_field.send_keys(username)
        username_next_button = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="identifierNext"]/div/button'))
        )
        # username_next_button = driver.find_element(
        #     By.XPATH, '//*[@id="identifierNext"]/div/button'
        # )
        username_next_button.click()
        time.sleep(5)
        password_field = WebDriverWait(driver, 30).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="password"]/div[1]/div/div[1]/input'))
        )
        # password_field = driver.find_element(
        #     By.XPATH, '//*[@id="password"]/div[1]/div/div[1]/input'
        # )
        # time.sleep(2)
        password_field.send_keys(password)
        password_next_button = WebDriverWait(driver, 30).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="passwordNext"]/div/button'))
        )
        # time.sleep(2)
        # password_next_button = driver.find_element(
        #     By.XPATH, '//*[@id="passwordNext"]/div/button'
        # )
        password_next_button.click()
        time.sleep(10)
